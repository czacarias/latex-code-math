\documentclass[11pt]{article}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\title{\huge{Chapter 1} \\ \Large{The Complex Plane and Elementary Functions}}
\author{Charlie Zacarias}
\date{\today}

\newtheorem{theorem}{Theorem}[section]
\numberwithin{equation}{section} % Allows enumeration of equations to be (section #).(equation #).
% New definition of square root.
% This renames \sqrt as \oldsqrt.
\let\oldsqrt\sqrt
% Below defines the new \sqrt in terms of the old one.
\def\sqrt{\mathpalette\DHLhksqrt}
\def\DHLhksqrt#1#2{%
	\setbox0=\hbox{$#1\oldsqrt{#2\,}$}\dimen0=\ht0
	\advance\dimen0-0.2\ht0
	\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
	{\box0\lower0.4pt\box2}}

\begin{document}
	\maketitle
	
	\section{Complex Numbers}
	A \textbf{complex number} is an expression of the form $z = x + iy$, where $x$ and $y$ are real numbers; in other words, $\mathbb{C} = \{z = x + iy\; |\; x,y \in \mathbb{R},\, i = \sqrt{-1}\}$. The component $x$ is called the \textbf{real part} of $z$, and $y$ is the \textbf{imaginary part} of $z$, and are denoted by
	\begin{displaymath}
		x\; =\; \text{Re}\; (z),
	\end{displaymath}
	\begin{displaymath}
		y\; =\; \text{Im}\; (z).
	\end{displaymath}
	\\
	The correspondence
	\begin{displaymath}
	z = x + iy \leftrightarrow (x,y)
	\end{displaymath}
	is a one-to-one correspondence between complex numbers and points (or vectors) in the Euclidean plain $\mathbb{R}^{2}$. The real numbers correspond to the x-axis in the Euclidean plane. The complex numbers that are the form $iy$ are called \textbf{purely imaginary numbers}, which form the imaginary axis $i\mathbb{R}$ in the complex plane and corresponds to the y-axis in the Euclidean plane.
	\subsection*{Adding Complex Numbers}
	To add complex numbers, we add their real and imaginary parts regarding their like-components. In other words $\text{Re}(z + w) = \text{Re}(z) + \text{Re}(w)$, and $\text{Im}(z + w) = \text{Im}(z) + \text{Im}(w)\; \forall \; z,w \; \in \mathbb{C}$:
		\begin{equation}
			(x + iy) + (u + iv) = (x + u) + i(y + v).
		\end{equation}
	
	\subsection*{Modulus of a Complex Number}
	The \textbf{modulus} of a complex number $z= x + iy$ is the length $\sqrt{x^2 + y^2}$ of the corresponding vector $(x,y)$ in the Euclidean plane. The modulus of $z$ is also called the \textbf{absolute value} of $z$, and it is denoted by $|z|$:
		\begin{equation}
			|z| = \sqrt{x^2 + y^2}.
		\end{equation}
	
	\subsection*{Triangle Inequality Property in $\mathbb{C}$}
	The triangle inequality for vectors in the plane takes the form
		\begin{equation}
			|z + w| \leq |z| + |w|, \qquad z,w \in \mathbb{C}.
		\end{equation}
	By \emph{applying} the triangle inequality to $z = (z - w) + w$, we obtain $|z| \leq |z - w| + |w|$. Subtracting $|w|$, a \textbf{useful} inequality is obtained,
		\begin{equation}
		|z - w| \geq |z| - |w|, \qquad z,w \in \mathbb{C}.
		\end{equation}
	
	\subsection*{Multiplying Complex Numbers \& Laws of Algebra}
	Complex numbers can be multiplied, and this is a feature that distinguishes the complex plane $\mathbb{C}$ from the Euclidean plane $\mathbb{R}^2$. Multiplication in $\mathbb{C}$ is defined by
		\begin{equation}
			(x + iy)(u + iv) = xu - yv + i(xv + yu), \quad \text{where}\; x,y,u,v \in \mathbb{R}\; \text{and}\; i = \sqrt{-1}.
		\end{equation} 
	In addition, the usual laws of algebra hold for complex multiplication:
		\begin{align}
			(z{_1}z{_2})z{_3} &= z{_1}(z{_2}{_3}), & \text{(associative law)} \\
			z{_1}z{_2} &= z{_2}z{_1}, &\text{(commutative law)} \\
			z{_1}(z{_2} + z{_3}) &= z{_1}z{_2} + z{_1}z{_3}. &\text{(distributive law)}
		\end{align}
	
	\subsection*{Multiplicative Inverse \& Complex Conjugate}
	Every complex number such that $z \not= 0$ has a \textbf{multiplicative inverse} $\frac{1}{z}$, which is explicitly defined as:
		\begin{equation}
			\frac{1}{z} = \frac{x - iy}{x^2 + y^2}\,, \qquad z = x + iy \in \mathbb{C},\; z \not= 0.
		\end{equation}
	For instance, the multiplicative inverse of $i$ is $\frac{1}{i} = -i$.\\ \\
	The \textbf{complex conjugate} of a complex number $z = x + iy$ is defined to be $\bar{z} = x - iy$. Geometrically, $\bar{z}$ is the reflection of $z$ in the $x$-axis. If reflected twice, z is returned,\\
		\begin{center}
			\begin{math}
			\bar{z} = z, \qquad z \in \mathbb{C}.
		\end{math}
		\end{center}
	Useful properties of complex conjugation are,
		\begin{align}
			\overline{z + w} &= \bar{z} + \bar{w}, & z,w \in \mathbb{C},\\
			\overline{zw} &= \bar{z}\bar{w}, & z,w \in \mathbb{C},\\
			|z| &= |\bar{z}|, & z \in \mathbb{C},\\
			|z|^2 &= z|\bar{z}|, & z \in \mathbb{C}.
		\end{align}	
	The last formula above allows $\frac{1}{z}$ to be expressed in terms of the complex conjugate $\bar{z}$:
		\begin{equation}
			\frac{1}{z} = \frac{\bar{z}}{|z|^2}, \qquad z \in \mathbb{C},\; z \not= 0.
		\end{equation}
	The real and imaginary parts of $z$ can be recovered from $z$ and $\bar{z}$, by
		\begin{align}
			\text{Re}(z) &= \frac{(z + \bar{z})}{2}, & z \in \mathbb{C}, \\
			\text{Im}(z) &= \frac{(z - \bar{z})}{2i}, & z \in \mathbb{C}.
		\end{align}
	We can obtain $|z||w|$ from $|zw|^2$ by the following,
		\begin{align*}
			\text{Let,}&\quad zw|^2 = (zw)(\overline{zw}); \quad |z|^2 = z\bar{z},\;\; \text{where}\, z = zw\\
			\text{Since,}& \quad \overline{zw} = \bar{z}\bar{w},\\ 
			\text{By the distributive property,}& \quad
			|zw|^2 = (zw)(\overline{zw}) = (zw)(\bar{z}\bar{w}) = (z\bar{z})(w\bar{w}),\\
			\text{By the multiplicative inverse,}& \quad (z\bar{z}) = |z|^2 \\  \text{and,}& \quad (w\bar{w}) = |w|^2, 
		\end{align*}
		\begin{align}
			\text{Therefore,}& \quad |zw| = |z||w|. \quad \square
		\end{align}
	
	\subsection*{Complex Polynomials}
	A \textbf{complex polynomial of degree} $n \geq 0$ is a function of the form
		\begin{equation}
			p(z) = a{_n}z^n + a_{n-1}z^{n-1} + \dots + a_{1}z + a_0, \qquad z \in \mathbb{C},\; a_o, \dots, a_n \in \mathbb{C},\; a_n \not= 0.
		\end{equation}
	The great thing about complex numbers, unlike real numbers, is that any polynomial with complex coefficients can be factored as a product of linear factors.
		\begin{theorem}[Fundamental Theorem of Algebra]
			Every complex polynomial p(z) of degree $n \geq 1$ has a factorization,
				\begin{equation*}
					p(z) = c(z - z_1)^{m_1} \dots (z - z_k)^{m_k},
				\end{equation*}
			where the ${z_j}$'s are distinct and $m_j \geq 1$. This factorization is unique, up to a permutation of the factors.
		\end{theorem}
	The uniqueness of the factorization is easy to establish. The points $z_1, \dots, z_k$ are uniquely characterized as the \textbf{roots}/\textbf{zeros} of $p(z)$. These are points where $p(z) = 0$. The integer $m_j$ is characterized as the unique integer $m$ with the property that $p(z)$ can be factored as $(z - z_j)^m q(z)$, where $q(z)$ is a polynomial satisfying $q(z_j) \not= 0$ (this is because if $q(z_j) = 0$, $p(z) = 0$ which has no unique factors). This theorem basically states that every complex polynomial of degree $n \geq 1$ has a zero.
\end{document}