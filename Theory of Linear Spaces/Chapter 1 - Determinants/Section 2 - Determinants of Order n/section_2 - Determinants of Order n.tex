\documentclass[11pt]{article}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\title{\huge{Chapter 1} \\ \Large{Determinants}}
\author{Charlie Zacarias}
\date{\today}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}

\newenvironment{definition}[1][Definition]{\begin{trivlist}
		\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}
\newenvironment{example}[1][Example]{\begin{trivlist}
		\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}
\newenvironment{remark}[1][Remark]{\begin{trivlist}
		\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}
\newcommand{\ind}{\hspace{0.25in}} % Allows indentation in an enviroment.

\numberwithin{equation}{section} % Allows enumeration of equations to be (section #).(equation #).
% New definition of square root.
% This renames \sqrt as \oldsqrt.
\let\oldsqrt\sqrt
% Below defines the new \sqrt in terms of the old one.
\def\sqrt{\mathpalette\DHLhksqrt}
\def\DHLhksqrt#1#2{%
	\setbox0=\hbox{$#1\oldsqrt{#2\,}$}\dimen0=\ht0
	\advance\dimen0-0.2\ht0
	\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
	{\box0\lower0.4pt\box2}}

\begin{document}
	\setcounter{section}{1}
	
	\section{Determinants of Order $n$}
	\subsection{Determinants}
	\ind Suppose we are given a \emph{square matrix}, that is, an $n \times n$ array of numbers $a_{ij}$ ($i,\,j = 1, 2, \dots, n$):
		\begin{equation}
			A_{n,n} =
			\begin{pmatrix}
			a_{11} & a_{12} & \cdots & a_{1n} \\
			a_{21} & a_{22} & \cdots & a_{2n} \\
			\vdots  & \vdots  & \ddots & \vdots \\
			a_{n1} & a_{n2} & \cdots & a_{nn}
			\end{pmatrix}
		\end{equation}
	The number of rows ($i$) and columns ($j$) of the matrix (2.1) is called its \textbf{order}. The numbers $a_{ij}$ are called the \textbf{elements} of the matrix. The first index indicates the row and the second index indicates the column in which $a_{ij}$ appears.\\
	\indent Any product of $n$ elements which appear in different rows and different columns of the matrix (2.1), i.e., a product containing \emph{just one element from each row and each column}. Such a product can be written in the form
		\begin{equation}
			a_{\alpha_1 1} \, a_{\alpha_2 2} \, \dots \, a_{\alpha_n n}
		\end{equation}
	For the first factor, we can always choose the element appearing in the first column of the matrix (2.1); then, if we denote by $\alpha_1$ the number of the row in which the element appears in, the indices of the element will be $\alpha_1, 1$, and so on. Thus, the indices $\alpha_1, \alpha_2, \dots, \alpha_n$ are the numbers of the rows in which the factors of the product (2.2) appear. Since the elements $a_{\alpha_1 1} \, a_{\alpha_2 2} \, \dots \, a_{\alpha_n n}$ appear in \emph{different} rows of the matrix (2.1), one from each row, then the numbers $\alpha_1, \alpha_2, \dots, \alpha_n$ are all different and represent some permutation of the numbers $1,2,\dots,n$.\\
	\indent An \textbf{inversion} in the sequence $\alpha_1, \alpha_2, \dots, \alpha_n$ means an arrangement of two indices such that the larger index comes before the smaller index. The total number of inversions will be denoted by $N(\alpha_1, \alpha_2, \dots, \alpha_n)$. For example, in the permutation $2,1,4,3$, there are two inversions (2 before 1, 4 before 3), so that
		\begin{equation*}
			N(2,1,4,3) = 2.
		\end{equation*}
	In the permutation $4,3,1,2$, there are five inversions (4 before 3, 4 before 1, 4 before 2, 3 before 1, 3 before 2), so that
		\begin{equation*}
			N(4,3,1,2) = 5.
		\end{equation*}
	If the number of inversions is \emph{even}, the product (2.2) is positive (a $+$ goes before the product); if the number is \emph{odd}, the product (2.2) is negative (a $-$ goes before the product). Otherwise denoted by the expression
		\begin{equation*}
			(-1)^{N(\alpha_1, \alpha_2, \dots, \alpha_n)}.
		\end{equation*}
	The total number of products in the form (2.2) which can be formed from the elements of a given matrix of order $n$ is equal to the total number of permutations of the numbers $1,2,\dots,n$. This number is equal to $n!$.
	
	\begin{definition}
		By the determinant $D$ of a matrix (2.1) is meant to be algebriac sum of the $n!$ products of the form (2.2), each of which is preceded by the sign determined by the rule $(-1)^{N(\alpha_1, \alpha_2, \dots, \alpha_n)}$.
		\begin{equation}
			D = \sum (-1)^{N(\alpha_1, \alpha_2, \dots, \alpha_n)}a_{\alpha_1 1} \, a_{\alpha_2 2} \, \dots \, a_{\alpha_n n}.
		\end{equation}
		And is denoted as
		\begin{equation}
			D = 
				\begin{vmatrix}
				a_{11} & a_{12} & \cdots & a_{1n} \\
				a_{21} & a_{22} & \cdots & a_{2n} \\
				\vdots  & \vdots  & \ddots & \vdots  \\
				a_{n1} & a_{n2} & \cdots & a_{nn}
				\end{vmatrix}
			  = \text{det}(a_{ij}).
		\end{equation}
	\end{definition}
	
	\indent For example, the following expressions for the determinants of orders two and three are:
		\begin{equation*}
			\begin{vmatrix}
			a_{1,1} & a_{1,2} \\
			a_{2,1} & a_{2,2}
			\end{vmatrix}
			= a_{11}a_{22} - a_{21}a_{12}
		\end{equation*}
	more recognizably,
		\begin{equation*}
			\begin{vmatrix}
			a & b \\
			c & d
			\end{vmatrix}
			= ad - cb
		\end{equation*}
	and of order three,
		\begin{equation*}
			\begin{vmatrix}
			a_{11} & a_{12} & a_{13} \\
			a_{21} & a_{22} & a_{23} \\
			a_{31} & a_{32} & a_{33}
			\end{vmatrix}
			=
			a_{11}\begin{vmatrix}
			a_{22} & a_{23} \\
			a_{32} & a_{33}
			\end{vmatrix}
			-
			a_{12}\begin{vmatrix}
			a_{21} & a_{23} \\
			a_{31} & a_{33}
			\end{vmatrix}
			+
			a_{13}\begin{vmatrix}
			a_{21} & a_{22} \\
			a_{31} & a_{32}
			\end{vmatrix}.
		\end{equation*}
		
	\subsection{Geometric description for Inversion}
	\ind The rule for determining the sign of a given term of a determinant can be formulated in geometric terms. Corresponding to the enumeration of the elements in a matrix like (2.1), we can distinguish two natural \textbf{positive directions}: from \emph{left to right} along the rows, and from \emph{top to bottom} along the columns. Moreover, the slanting lines (when you map it out) joining any two elements of the matrix can be furnished with a direction: the line segment joining the element $a_{ij}$ with the element $a_{kl}$ has a \emph{positive slope} if its right endpoint lies lower than its left endpoint, and that is has a \emph{negative slope} if its right endpoint lies higher than its left endpoint.\\
	\indent In the matrix (2.1) we draw all the segments with \emph{negative} slope joining pairs of elements $a_{\alpha_1 1} \, a_{\alpha_2 2} \, \dots \, a_{\alpha_n n}$ of the product (2.2). Then we put a plus sign before the product if the number of all such segments is even, and a minus sign if the number is odd. In a visual sense,
		\begin{equation}
			\begin{pmatrix}
			+ & - & + & \cdots \\
			- & + & - & \cdots \\
			+ & - & + & \cdots \\
			\vdots & \vdots & \vdots & \ddots
			\end{pmatrix}.
		\end{equation}
		
\end{document}