\documentclass[11pt]{article}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\title{\huge{Chapter 1} \\ \Large{Determinants}}
\author{Charlie Zacarias}
\date{\today}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}

\newenvironment{definition}[1][Definition]{\begin{trivlist}
		\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}
\newenvironment{example}[1][Example]{\begin{trivlist}
		\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}
\newenvironment{remark}[1][Remark]{\begin{trivlist}
		\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}
\newcommand{\ind}{\hspace{0.25in}} % Allows indentation in an enviroment.

\numberwithin{equation}{section} % Allows enumeration of equations to be (section #).(equation #).
% New definition of square root.
% This renames \sqrt as \oldsqrt.
\let\oldsqrt\sqrt
% Below defines the new \sqrt in terms of the old one.
\def\sqrt{\mathpalette\DHLhksqrt}
\def\DHLhksqrt#1#2{%
	\setbox0=\hbox{$#1\oldsqrt{#2\,}$}\dimen0=\ht0
	\advance\dimen0-0.2\ht0
	\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
	{\box0\lower0.4pt\box2}}

\begin{document}
	\maketitle
	
	\section{Systems of Linear Equations}
	\ind A \textbf{system of linear equations}, in the most general sense, has the form:
	
	\begin{equation}
		\begin{aligned}
		a_{11}x_1 + a_{12}x_2 + \dots + a_{1n}x_n &= b_1\\
		a_{21}x_1 + a_{22}x_2 + \dots + a_{2n}x_n &= b_2\\
		\cdots \qquad \qquad \quad &= \; \vdots\\
		a_{k1}x_1 + a_{k2}x_2 + \dots + a_{kn}x_n &= b_k
		\end{aligned}
	\end{equation}
	where $x_1 , x_2, \dots, x_n$ are unknowns to be determined.
	\begin{remark}
		Do not necessarily assume that the number of unknowns ($x_1 , x_2, \dots, x_n$) equals the number of coefficients ($a_{11}, a_{12}, \dots, a_{kn}$).
	\end{remark}
	\ind The quantities $a_{11}, a_{12}, \dots, a_{kn}$ are called the \textbf{coefficients} of the system.
	The quantities $b_1 , b_2 , \dots, b_k$ on the right-hand side of (1.1) are called the \textbf{constant terms} of the system; like the coefficients, they are assumed to be known. A solution of the system, in a general sense, is defined to be any set of numbers $c_1 , c_2 , \dots, c_n$ which when substituted for the unknowns $x_1 , x_2, \dots, x_n$, turns all the equations of the system into identities.
	\begin{remark}
		Not every system of linear equations has a solution of the form (1.1) however. For example,
		\begin{equation}
			\begin{aligned}
			2x_1 + 3x_2 &= 5\\
			2x_1 + 3x_2 &= 6
			\end{aligned}
		\end{equation}
		obviously has no solution at all since $x_1$ and $x_2$ cannot be the same while the constant terms are of different values. No substitution can simultaneously convert both equations of the system into identities.
	\end{remark}
	\ind A system of equations in the form of (1.1) which has \emph{at least} one solution is called \textbf{compatible}; whereas a system which \emph{does not} have solutions is called \textbf{incompatible}.
	
	\subsection*{Compatible Systems}
	\ind A compatible system can have one solution or several solutions. In the case of several solutions, the solutions are distinguished by indicated by, for example, the first solution is denoted as $c^{(1)}_1 , c^{(1)}_2, \dots, c^{(1)}_n$, the second solution by $c^{(2)}_1 , c^{(2)}_2, \dots, c^{(2)}_n$, and so on. These solutions are considered to be \textbf{distinct} if at least one of the numbers $c^{(1)}_i$ does not coincide with the corresponding numbers $c^{(2)}_i$ where $i = 1, 2, \dots, n$. For example, the system
		\begin{equation}
			\begin{aligned}
			2x_1 + 3x_2 = 0\\
			4x_1 + 6x_2 = 0
			\end{aligned}
		\end{equation}
	has the distinct solutions
		\begin{equation*}
			c^{(1)}_1 = c^{(1)}_2 = 0\quad \text{and}\quad c^{(2)}_1 = 3,\; c^{(2)}_2 = -2
		\end{equation*}
	(as well as infinitely many other solutions). If a compatible system has a unique solution (one solution), the system is called \textbf{determinate}; if a compatible system has at least two different solutions, it is called \textbf{indeterminate}.\\
	\textbf{Four basic points in studying systems in the form of (1.1) are:}
	\begin{enumerate}
		\item \textit{To ascertain whether the system is compatible or incompatible;}
		\item \textit{If the system is compatible, to ascertain whether it is determinate;}
		\item \textit{If the system is compatible and determinate, to find its unique solution, or}
		\item \textit{If the system is compatible and indeterminate, to describe the set of all its solutions.}
	\end{enumerate}
\end{document}